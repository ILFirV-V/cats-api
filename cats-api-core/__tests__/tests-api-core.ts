import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

export type Order = 'desc' | 'asc';
export type GenderCat = 'male' | 'femail';
export type CatInfo = {
    id: Number,
    name: String,
    description: String,
    tags: null,
    gender: GenderCat,
    likes: Number,
    dislikes: Number,
    count_by_letter: String,
}


const cats: CatMinInfo[] = [{ name: 'Южх', description: '', gender: 'male' }];

let catId;

const fakeId : string = 'fakeId';

const catDescription : string = "Новое Описание";
const orderQuery : Order = 'asc';
const limitQuery : number = cats[0].name.length;

const HttpClient = Client.getInstance();

describe('тесты на API метод get_by_id', () => {
    beforeAll(async () => {
        try {
        const add_cat_response = await HttpClient.post('core/cats/add', {
            responseType: 'json',
            json: { cats },
        });
        if ((add_cat_response.body as CatsList).cats[0].id) {
            catId = (add_cat_response.body as CatsList).cats[0].id;
        } else throw new Error('Не получилось получить id тестового котика!');
        } catch (error) {
        throw new Error('Не удалось создать котика для автотестов!');
        }
    });

    afterAll(async () => {
        await HttpClient.delete(`core/cats/${catId}/remove`, {
        responseType: 'json',
        });
    });

    it('Найти котика по id', async () => {
        const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`);
        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual(JSON.stringify({
            cat: 
                {
                    id: catId,
                    name: cats[0].name,
                    description: cats[0].description,
                    tags: null,
                    gender: cats[0].gender,
                    likes: 0,
                    dislikes: 0,
                }
        }));  
    });

    it('Попытка найти котика с некорректным id вызовет ошибку', async () => {
        await expect(
        HttpClient.get(`core/cats/get-by-id?id=${fakeId}`)
        ).rejects.toThrowError('Response code 400 (Bad Request)');
    });
});

describe('тесты на API метод save_description', () => {
    beforeAll(async () => {
        try {
        const add_cat_response = await HttpClient.post('core/cats/add', {
            responseType: 'json',
            json: { cats },
        });
        if ((add_cat_response.body as CatsList).cats[0].id) {
            catId = (add_cat_response.body as CatsList).cats[0].id;
        } else throw new Error('Не получилось получить id тестового котика!');
        } catch (error) {
        throw new Error('Не удалось создать котика для автотестов!');
        }
    });

    afterAll(async () => {
        await HttpClient.delete(`core/cats/${catId}/remove`, {
        responseType: 'json',
        });
    });

    it('Добавить описание', async () => {
        const response = await HttpClient.post(`core/cats/save-description`, {
            responseType: 'json',
            json: {
                "catId": catId,
                "catDescription": catDescription
            },
        });
  
        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual({
            id: catId,
            name: cats[0].name,
            description: catDescription,
            tags: null,
            gender: cats[0].gender,
            likes: 0,
            dislikes: 0,
        });  
    });
});


describe('тесты на API метод allByLetter', () => {
    beforeAll(async () => {
        try {
        const add_cat_response = await HttpClient.post('core/cats/add', {
            responseType: 'json',
            json: { cats },
        });
        if ((add_cat_response.body as CatsList).cats[0].id) {
            catId = (add_cat_response.body as CatsList).cats[0].id;
        } else throw new Error('Не получилось получить id тестового котика!');
        } catch (error) {
        throw new Error('Не удалось создать котика для автотестов!');
        }
    });

    afterAll(async () => {
        await HttpClient.delete(`core/cats/${catId}/remove`, {
        responseType: 'json',
        });
    });

    it('Получить группы', async () => {
        const response = await HttpClient.get(`core/cats/allByLetter?limit=${limitQuery}&order=${orderQuery}&gender=${cats[0].gender}`);
        expect(response.statusCode).toEqual(200);
        const responseBody = JSON.parse(response.body);
        expect(responseBody).toEqual({
            groups: expect.arrayContaining([
              {
                title: expect.any(String),
                cats: expect.arrayContaining([
                  expect.objectContaining({
                    id: expect.any(Number),
                    name: expect.any(String),
                    description: expect.any(String),
                    tags: null,
                    gender: expect.any(String),
                    likes: expect.any(Number),
                    dislikes: expect.any(Number),
                    count_by_letter: expect.any(String),
                  }),
                ]),
                count_in_group: limitQuery,
                count_by_letter: expect.any(Number),
              },
            ]),
            count_output: expect.any(Number),
            count_all: expect.any(Number),
        });
    });
});