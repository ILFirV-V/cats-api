const { swaggerBasePath, swaggerHost, swaggerSchemes } = require('./configs');

const swaggerScheme = {
  swagger: '2.0',
  info: { version: '', title: 'Cats API Core' },
  host: swaggerHost,
  basePath: swaggerBasePath,
  schemes: swaggerSchemes,
  paths: {
    '/cats/add': {
      post: {
        tags: ['Добавление'],
        summary: 'Добавить нового кота(ов) в список',
        description: 'Добавление нового кота(ов) в список',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [
          {
            in: 'body',
            name: 'body',
            description: 'Список объектов для добавления',
            required: true,
            schema: {
              type: 'object',
              properties: {
                cats: {
                  type: 'array',
                  description: 'Список котов для добавления',
                  items: {
                    type: 'object',
                    properties: {
                      name: {
                        description: 'Имя кота',
                        required: true,
                        type: 'string',
                        example: 'Котик',
                      },
                      description: {
                        description: 'Описание кота',
                        type: 'string',
                        example: 'Описание кота',
                      },
                      gender: {
                        required: true,
                        $ref: '#/definitions/GenderEnumRequired',
                      },
                    },
                  },
                },
              },
            },
          },
        ],
        responses: {
          200: {
            description: 'Резальтат добавления списка котов',
            schema: {
              type: 'object',
              properties: {
                cats: {
                  type: 'array',
                  items: { $ref: '#/definitions/Cat' },
                },
              },
            },
          },
        },
      },
    },
    '/cats/get-by-id': {
      get: {
        tags: ['Поиск'],
        summary: 'Найти кота по его ID',
        description: 'Поиск кота по его ID',
        parameters: [
          {
            name: 'id',
            in: 'query',
            description: 'Уникальный ID кота, по которому осуществляется поиск',
            required: true,
            type: 'number',
          },
        ],
        responses: {
          200: { description: 'Найденный по ID кот' },
          400: { description: 'Неверный формат ID' },
          404: { description: 'кот с данным ID не найден' },
        },
      },
    },
    '/cats/search': {
      post: {
        tags: ['Поиск'],
        summary: 'Найти кота по имени и доп.характеристикам',
        description: 'Поиск кота по имени и доп.характеристикам',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [
          {
            in: 'body',
            name: 'body',
            description: 'Объект с параметрами для поиска',
            required: true,
            schema: {
              type: 'object',
              properties: {
                name: {
                  type: 'string',
                  required: true,
                  example: 'кот',
                },
                gender: {
                  required: true,
                  $ref: '#/definitions/GenderEnum',
                },
                order: {
                  type: 'string',
                  description: 'Тип сортировки. Может принимать значения: asc, desc',
                  example: 'asc',
                  enum: ['asc', 'desc'],
                },
              },
            },
          },
        ],
        responses: {
          200: {
            description:
              'Список найденных котов сгруппированный и отсортированый в алфавитном порядке',
            schema: { $ref: '#/definitions/CatsGroups' },
          },
        },
      },
    },
    '/cats/search-pattern': {
      get: {
        tags: ['Поиск'],
        summary: 'Найти кота по части начала имени',
        description: 'Поиск кота по части начала имени',
        parameters: [
          {
            name: 'name',
            in: 'query',
            description: 'Часть начала имени',
            required: true,
            type: 'string',
          },
          {
            name: 'limit',
            in: 'query',
            description: 'Ограничение вывода количества имен',
            required: true,
            type: 'number',
            default: 10,
          },
          {
            name: 'offset',
            in: 'query',
            description: 'Смещение относительно начала списка имен',
            required: false,
            type: 'number',
          },
        ],
        responses: {
          200: {
            description: 'Список найденных котов',
            schema: {
              type: 'object',
              properties: {
                cats: {
                  type: 'array',
                  items: { $ref: '#/definitions/Cat' },
                },
              },
            },
          },
        },
      },
    },
    '/cats/save-description': {
      post: {
        tags: ['Добавление/обновление описания'],
        summary: 'Добавить опиание коту по ID',
        description: 'Добавление описания коту по ID',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [
          {
            in: 'body',
            name: 'body',
            description: 'Объект с параметрами для добавления описания',
            required: true,
            schema: {
              type: 'object',
              properties: {
                catId: {
                  description: 'Уникальный ID кота, по которому добавляется описание',
                  type: 'number',
                  example: 12,
                  required: true,
                },
                catDescription: {
                  description: 'Описание которое необходимо сохранить',
                  type: 'string',
                  example: 'Описание',
                  required: true,
                },
              },
            },
          },
        ],
        responses: {
          200: {
            description: 'кот для которого добавилось описание',
            schema: { $ref: '#/definitions/Cat' },
          },
          400: { description: 'Неверный формат ID' },
        },
      },
    },
    '/cats/validation': {
      get: {
        tags: ['Правила валидации'],
        summary: 'Получить правила валидации для имени',
        description: 'Список правил валидации для имени',
        responses: {
          200: {
            description: 'Список правил валидации',
            schema: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: { type: 'string', description: 'ID правила' },
                  description: { type: 'string', description: 'Текст правила' },
                  regex: { type: 'string', description: 'Регулярное выражение правила' },
                  type: { type: 'string', description: 'Тип правила' },
                },
              },
            },
          },
        },
      },
    },
    '/cats/all': {
      get: {
        tags: ['Поиск'],
        summary: 'Получить список всех котов',
        description: 'Список всех котов',
        deprecated: true,
        responses: {
          200: {
            description: 'Список всех котов сгруппированный и отсортированый в алфавитном порядке',
            schema: { $ref: '#/definitions/CatsGroups' },
          },
        },
      },
    },
    '/cats/allByLetter': {
      get: {
        tags: ['Поиск'],
        summary: 'Получить список котов сгруппированный по группам',
        description:
          'Список котов сгруппированный по группам с указанием количества котов в группе',
        parameters: [
          {
            name: 'limit',
            in: 'query',
            description: 'Количество котов в группе',
            required: false,
            type: 'number',
          },
          {
            name: 'order',
            in: 'query',
            description: 'Сортировка. Принимает значения - desc и asc',
            required: false,
            type: 'string',
          },
          {
            name: 'gender',
            in: 'query',
            description: 'Пол. Принимает значения - male и female',
            required: false,
            type: 'string',
          },
        ],
        responses: {
          200: {
            description: 'Список котов сгруппированный и отсортированый в алфавитном порядке',
            schema: { $ref: '#/definitions/CatsGroups' },
          },
        },
      },
    },
    '/cats/{catId}/remove': {
      delete: {
        tags: ['Удаление'],
        summary: 'Удалить кота по ID',
        description: 'Удаление кота по ID',
        parameters: [
          {
            name: 'catId',
            in: 'path',
            description: 'ID кота для удаления',
            required: true,
            type: 'number',
          },
        ],
        responses: {
          200: {
            description: 'Удаленный кот',
            schema: { $ref: '#/definitions/Cat' },
          },
          400: { description: 'Неверный формат ID' },
        },
      },
    },
  },
  definitions: {
    GenderEnumRequired: {
      type: 'string',
      description: 'Пол кота. Может принимать значения: male, female, unisex',
      enum: ['male', 'female', 'unisex'],
      example: 'unisex',
      required: true,
    },
    GenderEnum: {
      type: 'string',
      description: 'Пол кота. Может принимать значения: male, female, unisex',
      enum: ['male', 'female', 'unisex'],
      example: 'unisex',
    },
    Cat: {
      type: 'object',
      properties: {
        id: { type: 'integer', format: 'int32', example: 12 },
        name: { type: 'string', example: 'кот' },
        description: { type: 'string', example: 'Описание' },
        tags: { type: 'string', example: null },
        gender: { $ref: '#/definitions/GenderEnum' },
        likes: { type: 'integer', format: 'int32', example: 12 },
        dislikes: { type: 'integer', format: 'int32', example: 5 },
      },
    },
    CatsGroups: {
      type: 'object',
      description: 'Список котов сгруппированый по именам',
      properties: {
        groups: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              title: {
                type: 'string',
                description: 'Заголовок группы имён (первая буква имени)',
              },
              count: {
                type: 'number',
                description: 'Количество имён в группе',
              },
              cats: {
                type: 'array',
                items: { $ref: '#/definitions/Cat' },
              },
            },
          },
        },
      },
    },
  },
};

module.exports = {
  swaggerScheme,
};
